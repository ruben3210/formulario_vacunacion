<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" />

</head>

<body background="virus.jpg">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 mb-4">
                <div class="card m-4 border border-1 border-secondary">
                    <div class="card-header py-3">
                        <h5 class="mb-0">Registro vacunacion</h5>
                    </div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <div class="mb-2">
                                <label for="identificacion" class="form-label">Nº Identificacion</label>
                                <input type="number" class="form-control" id="identificacion" name="identificacion" required>
                            </div>
                            <div class="mb-2">
                                <label for="nombre" class="form-label">Nombres</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" required>
                            </div>
                            <div class="mb-2">
                                <label for="apellido" class="form-label">Apellidos</label>
                                <input type="text" class="form-control" id="apellido" name="apellido" required>
                            </div>
                            <div class="mb-2">
                                <label for="opc_vacuna" class="form-label">Tipo vacuna</label>
                                <select class="form-select" aria-label="Default select example" id="opc_vacuna" name="opc-vacuna" required>
                                    <option selected>Elija una opción</option>
                                    <option value="Moderna">Moderna</option>
                                    <option value="Pfizer">Pfizer</option>
                                    <option value="AstraZeneca">AstraZeneca</option>
                                    <option value="Johnson & Johnson">Johnson & Johnson</option>
                                    <option value="Sinovac">Sinovac</option>
                                </select>
                            </div>
                            <div class="row mb-4">
                                <div class="col-4">
                                    <label for="fecha1" class="form-label">Fecha primera dosis</label>
                                    <input type="date" id="fecha1" name="fecha1" require>
                                </div>
                                <div class="col-4">
                                    <label for="fecha2" class="form-label">Fecha segunda dosis</label>
                                    <input type="date" id="fecha2" name="fecha2">
                                </div>
                            </div>
                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                <input id="enviar-datos" class="btn btn-primary" type="submit" value="Enviar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($_POST["identificacion"])) {
        $salida = implode(";", $_POST) . chr(10);
    }

    if (isset($salida)) {
        $archivoDatos = fopen('tabla-vacunas.txt', 'a');
        fwrite($archivoDatos, $salida);
        fclose($archivoDatos);
    }
    ?>
    <div class="container">
        <div class="p-3 bg-secondary bg-gradient text-white">
            <h3 class="text-center ">REGISTRO VACUNACIÓN COVID-19</h3>
        </div>
        <table class="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col">N° Identificación</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Tipo de vacuna</th>
                    <th scope="col">Primera dosis</th>
                    <th scope="col">Segunda dosis</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $nombre_archivo = 'tabla-vacunas.txt';
                if (file_exists($nombre_archivo)) {
                    $archivo = fopen("tabla-vacunas.txt", "r");
                    while (!feof($archivo)) {
                        $fila = fgets($archivo);
                        $saltofila = nl2br($fila);
                        $datos = explode(";", $saltofila);
                        echo '<tr>';
                        for ($i = 0; $i < count($datos); $i++) {
                            echo '<td>' . $datos[$i] . '</td>';
                        }
                        echo '</tr>';
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</body>

</html>